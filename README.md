### Notes
---
- Replaces the sound that plays when you bump into anything with THAT boom sound effect.
- Multiplayer compatible. The sound is client-side, so there's zero risk of desyncs.

🗿